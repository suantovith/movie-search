const bodyParser = require('body-parser');
const express = require("express");
const fetch = require('node-fetch');
const path = require('path');
const cors = require('cors');
const {asyncWrapper, prepareString, isDate} = require("./libs/helpers")

const app = express();

const HTTP_PORT = 8000;
const API_URL = "https://api.themoviedb.org/3";
const API_KEY = "d0fd1d56698d6deea2d7285dd0507b23";

app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT));
    console.log("Enter http://localhost:%PORT%/ to access it.".replace("%PORT%",HTTP_PORT));
});

app.use(cors());
app.use(express.static('dist'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

async function getPopulars() {
    const response = await fetch(`${API_URL}/movie/popular?api_key=${API_KEY}`);
    const data = await response.json();

    return data;
}
app.get("/api/home", asyncWrapper(getPopulars));

async function getSearchResult(req) {
    let data = {};

    if(req.query.string != undefined && req.query.string != ""){
        data["movie"] = {
            result: {
                response:`Search movies with name <b>${req.query.string}</b>`,
                search: prepareString(req.query.string)
            }
        };

        const persons = await fetch(`${API_URL}/search/person?query=${prepareString(req.query.string)}&api_key=${API_KEY}`);
        data["persons"] = (await persons.json()).results;
        if(data["persons"] != undefined) data["persons"] = data["persons"].slice(0, 1);

        const genres = await fetch(`${API_URL}/genre/movie/list?&api_key=${API_KEY}`);
        const genres_list = await genres.json(); 
        let matches = [];
        genres_list.genres.forEach(function(item) {
            if(item.name.toLowerCase() == prepareString(req.query.string).toLowerCase() || item.name.toLowerCase().startsWith(prepareString(req.query.string).toLowerCase())){
                item.response = `Search movies with <b>${item.name}</b> genre</b>`;
                matches.push(item);
            }
        })
        if(matches.length>0) data["genres"] = matches;
    }

    if(isDate(req.query.string)){
        data["date_gte"] = {
            result: {
                response:`Search movies released after <b>${req.query.string}</b>`,
                search: prepareString(req.query.string)
            }
        };
        data["date_lte"] = {
            result: {
                response:`Search movies released before <b>${req.query.string}</b>`,
                search: prepareString(req.query.string)
            }
        };
    }

    return data;
}
app.get("/api/movie/result", asyncWrapper(getSearchResult));

async function getMoviesByDate(req){
    const dateObj = new Date(req.query.string);
    const newDate = `${dateObj.getUTCFullYear()}-${('0' + (dateObj.getUTCMonth() + 1)).slice(-2)}-${('0' + dateObj.getUTCDate()).slice(-2)}`;

    const response = await fetch(`${API_URL}/discover/movie?release_date.${req.query.mode=="gte"?"gte":"lte"}=${newDate}&api_key=${API_KEY}`);
    const data = await response.json();

    return data;
}
app.get("/api/movie/date", asyncWrapper(getMoviesByDate));

async function getMoviesByPerson(req){
    const response = await fetch(`${API_URL}/discover/movie?with_people=${req.query.id}&api_key=${API_KEY}`);
    const data = await response.json();

    return data;
}
app.get("/api/movie/person", asyncWrapper(getMoviesByPerson));

async function getMoviesByGenre(req){
    const response = await fetch(`${API_URL}/discover/movie?with_genres=${req.query.id}&api_key=${API_KEY}`);
    const data = await response.json();

    return data;
}
app.get("/api/movie/genre", asyncWrapper(getMoviesByGenre));

async function getMoviesByName(req){
    const response = await fetch(`${API_URL}/search/movie?query=${prepareString(req.query.string)}&api_key=${API_KEY}`);
    const data = await response.json();

    return data;
}
app.get("/api/movie", asyncWrapper(getMoviesByName));

async function getSingle(req) {
    const response = await fetch(`${API_URL}/movie/${req.query.id}?api_key=${API_KEY}`);
    const data = await response.json();

    return data;
}
app.get("/api/movie/single", asyncWrapper(getSingle));

async function getFavorites() {
    const response = await fetch(`${API_URL}/account?api_key=${API_KEY}`);
    const data = await response.json();

    const response_favorites = await fetch(`${API_URL}/account/${data.id}/favorite/movies?api_key=${API_KEY}`);
    const data_favorites = await response_favorites.json();

    return data_favorites;
}
app.get("/api/favorites", asyncWrapper(getFavorites));


async function addToFavorites(req) {
    
    const accounnt_response = await fetch(`${API_URL}/account?api_key=${API_KEY}`);
    const account_data = await accounnt_response.json();

    const response = await fetch(`${API_URL}/account/${account_data.id}/favorite?api_key=${API_KEY}`, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(req.body)
      });
    const data = await response.json();

    return data;
}
app.post("/api/favorites/add", asyncWrapper(addToFavorites));

app.get("/api/favorites/delete", (req, res) => {
    res.json({"":""})

    //TODO: Remove favorite here
});

app.get("/*", (req, res) => {
    res.sendFile(path.resolve(__dirname, 'dist', 'index.html'));
});

app.use(function(req, res){
    res.sendStatus(404);
});
