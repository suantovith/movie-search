module.exports.asyncWrapper = function(fn) {
    return (req, res, next) => {
      return Promise.resolve(fn(req))
        .then((result) => res.send(result))
        .catch((err) => next(err))
    }
  }
  
module.exports.prepareString = function(string){
    if(string == undefined || string == null){
        return "";
    }
    return escape(string.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/\u0142/g, "l"));
}

module.exports.isDate = function(date) {
    return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
}
