# Movie Search
## _Vue.js/Express.js based SPA_

movie-search requires [Yarn](https://yarnpkg.com/) to run.

Install the dependencies and packag:

```sh
yarn global add @vue/cli
yarn install
```

To start app run:
```sh
yarn run start
```

To build packages run:
```sh
yarn run build
```

To start only server run:
```sh
yarn run server
```
To start only front run:
```sh
yarn run test
```
