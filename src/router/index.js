import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Movie from '../views/Movie.vue'
import Favorites from '../views/Favorites.vue'

Vue.use(VueRouter)

const routes = [
  { path: "/favorites",
    name: "Favorites",
    component: Favorites
  },
  {
    path: "/:id",
    name: "Movie",
    component: Movie
  },
  {
    path: "/",
    name: "Home",
    component: Home
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
