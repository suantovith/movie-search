import 'whatwg-fetch';

async function getData(url = '') {
  const response = await fetch(`http://localhost:8000${url}`, {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
  });
  const data = await response.json();
  return data;
}
async function postData(url = '', post = {}) {
  const response = await fetch(`http://localhost:8000${url}`, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify(post)
  });
  const data = await response.json();
  return data;
}

function convertResponse(data){
  let arr = new Array();
  if(data.results != undefined){
    data.results.forEach(function(item) {
      arr.push({
        title: item.title,
        id: item.id,
        overview: item.overview,
        rating: `Average rating: ${item.vote_average}`,
        avatar: `https://image.tmdb.org/t/p/original${item.poster_path}`
      });
    });
  }
  return arr;
}

export {convertResponse, getData, postData};